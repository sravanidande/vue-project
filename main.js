Vue.component('product',{
    props:{
        premium:{
            type:Boolean,
            required:true
        }
    },
    template:`
    <div class="product">
            <div class="product-image">
                <img v-bind:src="image">
            </div> 
            <div class="product-info">
                    <h1>{{title}}</h1>
                    <p v-if="inStock">In Stock</p>
                    <p v-else>Out Of Stock</p>
                    <p>Shipping:{{shipping}}</p>
                    <!-- <p v-else-if="inventory < 10 && inventory > 0"></p>Almost Sold Out</p>
                    <p v-else>Out Of Stock</p> -->

                    <ul>
                        <li v-for="detail in details">{{detail}}</li>
                    </ul>

                    <div v-for="(varient,index) in varients" 
                    :key="varient.varientId" 
                    class="color-box" 
                    :style="{backgroundColor:varient.varientColor}"
                    @mouseover="updateProduct(index)">
                     

                        </div>

                        <button @click="addToCart" 
                        :disabled="!inStock"
                        :class="{disabledButton:!inStock}">Add to cart</button>

                        
            </div>

          
           
        <product-review @review-submitted="addReview"></product-review>
    
        </div>` ,
        data(){
            return {
                brand:"vue Matserful Socks", 
            product:'socks',
            selectedVarient:0,
            details:["80% cotton","20% ployster","Gender-neutral"],
            varients:[{
                varientId:2234,
                varientColor:"green",
                varientImage:"./images/green-onWhite.jpg",
                varientQuantity:10
            },
            {
            varientId:2236,
            varientColor:"blue",
            varientImage:"./images/blue-onWhite.jpg",
            varientQuantity:0
            }],
            reviews:[]
            
        }
        },
        methods:{ 
            addToCart(){  
               this.$emit('add-to-cart', this.varients[this.selectedVarient].varientId )
            },
            updateProduct(index){
                this.selectedVarient=index
                console.log(index)
            },
            addReview(productReview){
                this.reviews.push(productReview)
            }
        },
        computed:{
            title(){
                return this.brand+""+this.product
            },
            image(){
                return this.varients[this.selectedVarient].varientImage
            },
            inStock(){
                return this.varients[this.selectedVarient].varientQuantity
            },
            shipping(){
                if(this.premium){
                    return "Free"
                }
                return 2.99
            }
        }
    
         

    
    
})

Vue.component('product-review',{
    template:`
    <form class="review-form" @submit.prevent="onSubmit">

<p v-if="errors.length">
<b>please correct the following errors(s):</b>
<ul>
<li v-for="error in errors">{{error}}</li>
</ul>
<p>

      <p>
        <label for="name">Name:</label>
        <input id="name" v-model="name" placeholder="name">
      </p>
      
      <p>
        <label for="review">Review:</label>      
        <textarea id="review" v-model="review" ></textarea>
      </p>
      
      <p>
        <label for="rating">Rating:</label>
        <select id="rating" v-model.number="rating">
          <option>5</option>
          <option>4</option>
          <option>3</option>
          <option>2</option>
          <option>1</option>
        </select>
      </p>
          
      <p>
        <input type="submit" value="Submit">  
      </p>    
    
    </form>

    `,
    data(){
        return{
            name:null,
            review:null,
            rating:null,
            errors:[]
        }
    },
    methods:{
        onSubmit(){
            if(this.name&&this.review&&this.rating){
            let productReview = {
                name:this.name,
                review:this.review,
                rating:this.rating
            }
            this.$emit("review-submitted",productReview)
            this.name=null,
            this.review=null,
            this.rating=null
        }
        else{
            if(!this.name)this.errors.push("Name Required.")
            if(!this.review)this.errors.push("Review Required.")
            if(!this.rating)this.errors.push("Rating Required.")
        }
    }
    }
})

const app = new Vue({
    el:'#app', 
    data:{
        premium:false,
        cart:[]
    },
    methods:{
        updateCart(id){
            this.cart.push(id)
        }
    }
   
    
    
})